const { app, BrowserWindow, shell } = require('electron')
const path = require('path');
const express = require('express')
const router = express.Router();
const {createServer} = require("http");
const fs = require('fs')
const {Server} = require('socket.io');

const webserver = express()
const httpServer = createServer(webserver)

webserver.use('/', express.static(__dirname+'/public'));

router.get('/barre',function(req,res){
  res.sendFile(path.join(__dirname+'/public/barre.html'));
});
router.get('/screen',function(req,res){
  res.sendFile(path.join(__dirname+'/public/screen.html'));
});
router.get('/bandeau',function(req,res){
  res.sendFile(path.join(__dirname+'/public/bandeau.html'));
});
router.get('/bandeau2',function(req,res){
  res.sendFile(path.join(__dirname+'/public/bandeau-2.html'));
});

webserver.use('/', router)

const io = new Server(httpServer)

fs.readFile('./config.json', 'utf8', async (err, data) => {
  if (err) {
    console.error(err)
    return
  }

  const config =  JSON.parse(data)

  showRecords = config.show
  record = config.record
  max = config.max
  filepath = config.filepath
})

let showRecords = false
let record = 0
let max = 130
let filepath = ''

let readInterval = setInterval(() => {
  if (!filepath) return
  fs.readFile(filepath, 'utf8', (err, data) => {
    if (err) {
      console.error(err)
      return
    }
    
    io.emit('current', data)
  })
}, 100)

io.on('connection', client => {
  console.log('Client connected')

  client.emit('config', {
    filepath,
    record,
    max,
    show: showRecords
  })

  client.emit('max', max)
  client.emit('recordsState', showRecords)
  client.emit('kc', record)

  client.on('toggleRecords', show => {
    showRecords = show
    updateConfigFile()
    io.emit('toggleRecords', show)
  })

  client.on('kc', score => {
    record = score
    updateConfigFile()
    io.emit('kc', score)
  })

  client.on('max', score => {
    max = score
    updateConfigFile()
    io.emit('max', score)
  })

  client.on('file', newfilepath => {
    filepath = newfilepath
    clearInterval(readInterval)
    readInterval = setInterval(() => {
      if (!filepath) return
      fs.readFile(filepath, 'utf8', (err, data) => {
        if (err) {
          console.error(err)
          return
        }
        
        io.emit('current', data)
      })
    }, 100)
    updateConfigFile()
  })

  client.on('disconnect', () => {
    console.log('Client disconnected')
  })
})

function createWindow () {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    autoHideMenuBar: true
  })

  win.loadFile('./public/control.html')

  win.webContents.on('new-window', function(e, url) {
    e.preventDefault();
    require('electron').shell.openExternal(url);
  });
}

app.whenReady().then(() => {
  createWindow()

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow()
    }
  })
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
    httpServer.close()
  }
})

httpServer.listen(2022, () => {
  console.log('Server running on port 2022')
})

function updateConfigFile() {
  const config = JSON.stringify({
    filepath,
    record,
    max,
    show: showRecords
  });
  fs.writeFile('./config.json', config, (err) => {
    if (err) console.error(err);
  });
}